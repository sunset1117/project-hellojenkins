pipeline {
    agent any

    environment {
        VERSION = sh(
                returnStdout: true,
                script: "grep version package.json | awk '{print \$2}' | sed  's/[\"|,]//g'"
            )
        REGISTRY_URL = 'registry.gitlab.com'
        REGISTRY_PROJ = 'yjring/project-hellojenkins'
    }

    stages {
        stage('build') {
            steps {
                sh 'echo "Building.., app version: $VERSION"'
                sh 'docker build --no-cache -t $JOB_NAME:$BUILD_NUMBER .'
            }
        }

        stage('test') {
            parallel {
                stage('unit_test') {
                    steps {
                        echo 'Running unit tests..'
                        sh 'docker run --rm $JOB_NAME:$BUILD_NUMBER /bin/sh -c "npm install --include=dev && npm test"'
                    }
                }

                stage('api_test') {
                    steps {
                        echo 'Running api tests..'
                        sh 'docker run --rm $JOB_NAME:$BUILD_NUMBER /bin/sh -c "npm install --include=dev && npm run testapi"'
                    }
                }

                // stage('e2e_test') {
                //     steps {
                //         echo 'Running E2E tests..'
                //         sh '''
                //             docker run --rm -e NODE_ENV=production -e SELENIUM_SERVER=host -e PORT=3000 \
                //                 -v ${PWD}:/app -w /app registry.gitlab.com/cphtofficial/image-nodejs-chrome:1.0.0 \
                //                 /bin/bash -c "npm install --include=dev && npm run teste2e"
                //         '''
                //     }
                // }
            }
        }

        stage('publish') {
            when {
                expression { env.GIT_BRANCH == 'origin/main' }
            }
            steps {
                echo 'Publishing image..'
                withCredentials([usernamePassword(credentialsId: 'gitlab-project-hellojenkins-token', usernameVariable: 'REGISTRY_USER', passwordVariable: 'REGISTRY_TOKEN')]) {
                    sh 'echo $REGISTRY_TOKEN | docker login -u $REGISTRY_USER --password-stdin $REGISTRY_URL'
                    sh 'docker tag $JOB_NAME:$BUILD_NUMBER $REGISTRY_URL/$REGISTRY_PROJ:$VERSION'
                    sh 'docker push $REGISTRY_URL/$REGISTRY_PROJ:$VERSION'
                }
            }
        }

        stage('deploy') {
            when {
                expression { env.GIT_BRANCH == 'origin/main' }
            }
            agent {
                docker {
                    image 'cytopia/ansible:2.10'
                    args '-u root:root'
                }
            }
            steps {
                echo 'Deploying app..'
                withCredentials([
                    sshUserPrivateKey(credentialsId: 'production-auth', keyFileVariable: 'AUTH_KEY', usernameVariable: 'AUTH_USER'),
                    usernamePassword(credentialsId: 'gitlab-project-hellojenkins-token', usernameVariable: 'REGISTRY_USER', passwordVariable: 'REGISTRY_TOKEN')
                ]) {
                    sh '''
                        export ANSIBLE_HOST_KEY_CHECKING=False
                        ansible-playbook -i deployments/ansible/inventory -u $AUTH_USER --private-key $AUTH_KEY deployments/ansible/deploy.yml
                    '''
                }
            }
        }
    }

    post {
        cleanup {
            echo 'Cleanup..'
            script {
                if (env.GIT_BRANCH == 'origin/main') {
                    sh 'docker rmi $REGISTRY_URL/$REGISTRY_PROJ:$VERSION'
                }
                sh 'docker rmi $JOB_NAME:$BUILD_NUMBER'
            }
        }
    }
}