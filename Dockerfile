FROM node:18.16.0-alpine3.18

# Set environment variables for app
ENV NODE_ENV=production
ENV PORT=3000

# Create app group and user
RUN addgroup -S appgroup && adduser -S appuser -G appgroup

# Create app folder
ENV APP_HOME=/app
RUN mkdir $APP_HOME
RUN chown -R appuser:appgroup $APP_HOME
RUN echo ${JOB_NAME}
WORKDIR $APP_HOME

# Run as appuser
USER appuser

# Install dependencies
COPY --chown=appuser:appgroup . $APP_HOME
RUN npm install

# Expose port
EXPOSE 3000

# Start application
CMD ["npm", "start"]
