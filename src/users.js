/**
 * DO NOT use THIS in your Production Code!!!
 * DO NOT use THIS in your Production Code!!!
 * DO NOT use THIS in your Production Code!!!
 */
const mysql = require('mysql');

function getProfile(username) {
    const connection = mysql.createConnection({
        host: process.env.DB_HOST,
        user: process.env.DB_USER,
        password: process.env.DB_PASSWORD,
        database: process.env.DB_DATABASE
    });

    return new Promise((resolve, reject) => {
        // Sensitive Code Example (SQL injection)
        //   If an attacker inputs:
        //       mary' OR 1=1 OR '
        connection.query(`SELECT * FROM users WHERE username = '${username}'`, (err, res) => {
            if (err) {
                console.error(err);
                reject(err.code);
            } else {
                let profile = [];
                if (res.length > 0) {
                    res.forEach(user => {
                        profile.push({ 'name': user.name, 'birth': user.birth });
                    });

                }
                resolve(profile);
            }
        });
    })

}

exports.getProfile = getProfile;